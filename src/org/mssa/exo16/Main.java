package org.mssa.exo16;


import java.util.ArrayList;
import java.util.List;


public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String filename = ("files/Personnes.txt");

		// Q1 a) creer buffers en memoire : ByteArrayOutputStream
		//    b) flux pour ecrire types primitifs Java et chaines de car dans fichiers
		// binaires : DataOutputStream

		PersonWriter pw = new PersonWriter();
		PersonReader pr = new PersonReader();
		
		//Q2
		Person p1 = new Person();
		Person p2 = new Person();
		Person p3 = new Person();
		p1.setLastName("Jordan");
		p1.setFirstName("Micheal");
		p1.setAge(50);
		p2.setLastName("Paul");
		p2.setFirstName("Chris");
		p2.setAge(32);
		p3.setLastName("Johnson");
		p3.setFirstName("Magic");
		p3.setAge(60);

		List<Person> personnes = new ArrayList<Person>();
		personnes.add(p1);
		personnes.add(p2);
		personnes.add(p3);		
		
		pw.writeBinaryFields(personnes, filename);

		//Q3 : a) pas besoin de relire fichier tableau d'octets par tableau d'octets
		//     b) pr relire bn nb de pers : 

		//pr.readBinaryFields(filename);
		System.out.println(pr.readBinaryFields2(filename));
		
	}

}
