package org.mssa.exo16;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PersonReader {

	public void readBinaryFields(String filename) {
		FileInputStream fis;
		BufferedInputStream bis;
		try {

			fis = new FileInputStream(new File(filename));
			bis = new BufferedInputStream(new FileInputStream(new File(filename)));
			
			System.out.println(bis.available());
			int n = bis.read();
			while(n>=0) {
				System.out.print((char) n);
				n = bis.read();
			}
			
			
		
			fis.close();
			bis.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
    public List<Person> readBinaryFields2 (String filename){
        
        try (FileInputStream fis = new FileInputStream(filename)) {
                DataInputStream dis = new DataInputStream(fis);
                List<Person> personnes = new ArrayList<Person>();
                
                int i;
                int n = dis.read();
                System.out.println("Nb personnes : " + n);
                for(i=0; i<n;i++){
                        Person p = new Person();
                        try { 
                                p.setFirstName(dis.readUTF());
                                p.setLastName(dis.readUTF());
                                p.setAge(dis.readInt());                
                        } catch (IOException e) {
                                e.getMessage();
                        }
                        personnes.add(p);
                }
                                
        dis.close();
        return personnes;
        } catch (IOException e){
                System.out.println(e.getMessage());
        }
        
        
        return null;
        
        
}

}
