package org.mssa.exo16;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.function.Function;

public class PersonWriter {

	Function<Person, byte[]> fByte = p -> {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(bos);

		try {
			dos.writeUTF(p.getLastName());
			dos.writeUTF(p.getFirstName());
			dos.writeInt(p.getAge());

		} catch (IOException e) {
			e.printStackTrace();
		}
		return bos.toByteArray();

	};

	public void writeBinaryFields(List<Person> people, String fileName) {

		try {
			FileOutputStream fos = new FileOutputStream(new File(fileName), true);
			BufferedOutputStream bos = new BufferedOutputStream(fos);

			bos.write(people.size());
			people.stream().map(fByte).forEach(b -> {
				try {

					bos.write(b);
				}

				catch (IOException e) {
					e.printStackTrace();
				}

			});
			bos.close();

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}
}
