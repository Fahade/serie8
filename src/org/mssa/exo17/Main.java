package org.mssa.exo17;

import java.util.ArrayList;
import java.util.List;


public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String filename = ("files/Personnes2.ser");

		PersonWriter pw = new PersonWriter();
		PersonReader pr = new PersonReader();
		
		/*Person p1 = new Person();
		Person p2 = new Person();
		Person p3 = new Person();
		p1.setLastName("Jordan");
		p1.setFirstName("Micheal");
		p1.setAge(50);
		p2.setLastName("Paul");
		p2.setFirstName("Chris");
		p2.setAge(32);
		p3.setLastName("Johnson");
		p3.setFirstName("Magic");
		p3.setAge(60);*/

		Person p1 = new Person("Jordan","Micheal",50);
		Person p2 = new Person("Paul","Chris",32);
		Person p3 = new Person("Johnson","Magic",60); 
		
		List<Person> personnes = new ArrayList<Person>();
		personnes.add(p1);
		personnes.add(p2);
		personnes.add(p3);
		
		
		// Q1 : Utilisation du flux ObjectOutputStream pour �crire objets Java ds fichiers binaires
		
		// Q2 : La classe Person doit impl�menter l'interface Serializable pr �crire ses instances direct ds fichier bin

		// Q5
		pw.writeBinaryObject(personnes, filename);
		
		System.out.println(pr.readBinaryObject(filename));

	}

}
