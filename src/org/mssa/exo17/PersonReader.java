package org.mssa.exo17;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;


public class PersonReader {

	
	public List<Person> readBinaryObject(String fileName) {
        List<Person> personnes = new ArrayList<Person>();
        int nb = 0;
		try
		{
		    ObjectInputStream ois = new ObjectInputStream (new FileInputStream (new File(fileName)));
		    nb = ois.readInt();
		    for (int i= 0;i<nb;i++) {
		    	personnes.add((Person) ois.readObject());
		    }
		    ois.close();
		 
		}
		catch (ClassNotFoundException exception)
		{
		    System.out.println ("Impossible de lire l'objet : " + exception.getMessage());
		}
		catch (IOException exception)
		{
		    System.out.println ("Erreur lors de l'�criture : " + exception.getMessage());
		}
		
		return personnes;
	}
}
