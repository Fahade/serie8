package org.mssa.exo17;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

public class PersonWriter {
	
	public void writeBinaryObject(List<Person> people, String fileName) {
		ObjectOutputStream oos = null;
		int nb = people.size();
		try
		{
		    oos =  new ObjectOutputStream (new FileOutputStream (new File(fileName)));
			oos.writeInt(nb);
		    for(Person p : people) oos.writeObject (p);
		    oos.close();
		}
		catch (IOException exception)
		{
		    System.out.println ("Erreur lors de l'�criture : " + exception.getMessage());
		}
	}

}
